import React from "react"
import { Link } from "gatsby"

import Layout from '../components/layout';

export default () => (
  <Layout>
    <h1>Contact</h1>
    <p>Contact page content</p>
  </Layout>
);