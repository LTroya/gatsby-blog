import React from 'react';
import { Link, graphql, useStaticQuery } from "gatsby"

import Layout from '../components/layout';

export default () => {
  const data = useStaticQuery(graphql`
   query {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              title
              date
            }
            fields {
              slug
            }
          }
        }
      }
   }
  `);
  return  (
    <Layout>
      <h1>Blog</h1>
      <ol>
        {data.allMarkdownRemark.edges.map(edge => (
          <li key={edge.node.frontmatter.title}>
            <h3>
              <Link to={`/blog/${edge.node.fields.slug}`}>{edge.node.frontmatter.title}</Link>
            </h3>
            <p>{edge.node.frontmatter.date}</p>
          </li>
        ))}
      </ol>
    </Layout>
  )

}