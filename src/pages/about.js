import React from "react"
import { Link } from "gatsby"

import Layout from '../components/layout';

export default () => (
  <Layout>
    <h1>About</h1>
    <p>About page content</p>
    <p><Link to="/contact">Want to work with me?</Link></p>
  </Layout>
);