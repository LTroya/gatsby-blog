import React from "react"
import { Link } from "gatsby"

import Layout from '../components/layout';

export default () => (
  <Layout>
    <p>Need a developer? <Link to="/contact">Contact me.</Link></p>
  </Layout>
)